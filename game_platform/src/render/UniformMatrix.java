package render;

import java.nio.FloatBuffer;
import math.Matrix4;

/**
 * @author David Gronlund
 */
public class UniformMatrix implements Uniform {

    private Matrix4 matrix;

    public UniformMatrix(Matrix4 matrix) {
        this.matrix = new Matrix4(matrix);
    }

    @Override
    public int getType() {
        return Uniform.UNIFORM_MATRIX;
    }

    public FloatBuffer getData() {
        return matrix.toBuffer();
    }
}
