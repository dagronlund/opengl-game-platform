#version 420

uniform int cameraLight;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform vec4 lightPos;

in vec4 inVertex;
in vec4 inNormal;

out vec3 normal;
out vec3 lightDirection;
out float lightDistance;

void main() {
    mat3 normalMatrix = transpose(inverse(mat3(mvMatrix)));
    normal = normalMatrix * vec3(inNormal);
    if (cameraLight == 0) {
        lightDirection = vec3(normalize(mvMatrix * (lightPos -  inVertex)));
        lightDistance = length(lightPos -  inVertex);
    } else {
        lightDirection = vec3(normalize(mvMatrix * -inVertex));
        lightDistance = 0;
    }
    gl_Position = (pMatrix * mvMatrix) * inVertex;
}