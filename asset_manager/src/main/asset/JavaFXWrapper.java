/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.asset;

import java.io.File;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author David
 */
public class JavaFXWrapper extends Application implements Runnable, EventHandler<ActionEvent> {

    File file;
    Label labelFile;

    @Override
    public void run() {
        launch();
    }
    Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        labelFile = new Label("No file is open");
        Button btn = new Button();
        btn.setText("Select File");
        btn.setOnAction(this);
        TextField text = new TextField();
        text.setText("Hello!");
        text.setOnKeyPressed(fieldValidate);

        text.setLayoutX(10);
        text.setLayoutY(10);
        text.setTranslateY(20);

        VBox box = new VBox();
        box.getChildren().addAll(labelFile, btn, text);

        StackPane root = new StackPane();
        root.getChildren().add(box);
        stage.setScene(new Scene(root, 300, 250));
        stage.show();
    }

    @Override
    public void handle(ActionEvent event) {
        FileChooser dialog = new FileChooser();
        if (file != null) {
            File startDir = file.getParentFile();
            dialog.setInitialDirectory(startDir);
        }
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Asset Files (*.ten)", "*.ten");
        dialog.getExtensionFilters().add(extFilter);
        file = dialog.showOpenDialog(stage);
        labelFile.setText(file.getPath());
    }
    private EventHandler<KeyEvent> fieldValidate = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.ENTER) {
                System.out.println("Test");
            }
        }
    };
}
