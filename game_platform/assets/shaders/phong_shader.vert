#version 420

uniform int cameraLight;
uniform mat4 mMatrix;
uniform mat4 vMatrix;
uniform mat4 pMatrix;
uniform vec4 lightPos;

in vec4 inVertex;
in vec4 inNormal;

out vec3 normal;
out vec3 lightDirection;
out vec4 vertex;

void main() {
    mat4 mvMatrix = vMatrix * mMatrix;
    mat3 normalMatrix = transpose(inverse(mat3(mvMatrix)));
    normal = normalMatrix * vec3(inNormal);
    vertex = inVertex;
    if (cameraLight == 0) {
        lightDirection = vec3(normalize(vMatrix * lightPos - mvMatrix * inVertex));
    } else {
        lightDirection = vec3(normalize(mvMatrix * -inVertex));
    }
    gl_Position = (pMatrix * mvMatrix) * inVertex;
}