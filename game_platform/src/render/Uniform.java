package render;

/**
 * @author David Gronlund
 */
public interface Uniform {

    public int UNIFORM_VECTOR = 0;
    public int UNIFORM_MATRIX = 1;
    
    public int getType();
}
