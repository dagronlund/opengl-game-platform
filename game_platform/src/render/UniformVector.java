package render;

import math.Vector4;

/**
 * @author David Gronlund
 */
public class UniformVector implements Uniform {

    private Vector4 vector;

    public UniformVector(Vector4 vector) {
        this.vector = new Vector4(vector);
    }

    @Override
    public int getType() {
        return Uniform.UNIFORM_VECTOR;
    }

    public float getW() {
        return vector.w;
    }

    public float getX() {
        return vector.x;
    }

    public float getY() {
        return vector.y;
    }

    public float getZ() {
        return vector.z;
    }
}
