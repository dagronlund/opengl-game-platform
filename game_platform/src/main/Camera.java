package main;

import math.*;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

/**
 * @author David Gronlund
 */
public class Camera {

    private Vector4 translation = new Vector4(0, 0, 0, 1);
    private float rotateX;
    private float rotateY;
    private float rotateZ;
    private Matrix4 viewMatrix;
    private Matrix4 projectionMatrix;

    public Camera(float near, float far, float fov, float aspect) {
        viewMatrix = new Matrix4().setIdentity();
        projectionMatrix = GameMath.projectionMatrix(near, far, fov, aspect);
    }

    public Matrix4 getViewMatrix() {
        return new Matrix4(viewMatrix);
    }

    public Matrix4 getProjectionMatrix() {
        return new Matrix4(projectionMatrix);
    }

    public Matrix4 getViewProjectionMatrix() {
        return new Matrix4(projectionMatrix).multiply(viewMatrix);
    }

    public void handleInput() {
        float speed = 1f;
        Vector4 keysPressed = new Vector4();
        int dx = Mouse.getDX();
        int dy = Mouse.getDY();
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            keysPressed.z += speed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            keysPressed.z -= speed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            keysPressed.x -= speed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            keysPressed.x += speed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) {
            keysPressed.y -= speed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
            keysPressed.y += speed;
        }
        keysPressed.rotate(new Vector4(0, 1, 0), -rotateY);
        translation.translate(keysPressed);
        rotateY -= GameMath.toRadians(dx / 16.0f);
        rotateY = GameMath.normalize(rotateY);
        rotateX -= GameMath.toRadians(dy / 16.0f);
        rotateX = GameMath.normalize(rotateX);
        viewMatrix.setIdentity();
        viewMatrix.translate(translation);
        viewMatrix.rotate(0, 1, 0, rotateY);
        viewMatrix.rotate(1, 0, 0, -rotateX);
        viewMatrix = GameMath.findCameraMatrix(viewMatrix, false);
    }
}
