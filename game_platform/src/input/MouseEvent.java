package input;

import org.lwjgl.input.Mouse;

/**
 * @author David Gronlund
 */
public class MouseEvent {
    
    public final int x;
    public final int y;
    public final int dx;
    public final int dy;
    public final int dwheel;
    public final int button;
    public final long nanoseconds;

    public MouseEvent() {
        x = Mouse.getEventX();
        y = Mouse.getEventY();
        dx = Mouse.getEventDX();
        dy = Mouse.getEventDY();
        dwheel = Mouse.getEventDWheel();
        button = Mouse.getEventButton();
        nanoseconds = Mouse.getEventNanoseconds();
    }
}
