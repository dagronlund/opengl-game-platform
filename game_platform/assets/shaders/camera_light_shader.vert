uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform vec4 vColor;
varying vec4 outFragColor;
attribute vec4 inVertex;
attribute vec4 inNormal;
void main(void) {
    vec3 newNorm = vec3(inNormal);
    mat3 mNormalMatrix;
    mNormalMatrix[0] = mvMatrix[0].xyz;
    mNormalMatrix[1] = mvMatrix[1].xyz;
    mNormalMatrix[2] = mvMatrix[2].xyz;
    vec3 vNorm = normalize(mNormalMatrix * newNorm);
    vec3 vLightDir = vec3(0.0, 0.0, 1.0);
    float fDot = max(0.0, dot(vNorm, vLightDir)) / 2.0 + 0.5;
    outFragColor.rgb = vColor.rgb * fDot;
    outFragColor.a = vColor.a;
    mat4 mvpMatrix;
    mvpMatrix = pMatrix * mvMatrix;
    gl_Position = mvpMatrix * inVertex;
}