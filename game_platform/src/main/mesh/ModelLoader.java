package main.mesh;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author David Gronlund
 */
public abstract class ModelLoader {

    private AtomicBoolean done = new AtomicBoolean(false);
    private AtomicBoolean running = new AtomicBoolean(false);

    protected void done() {
        running.set(false);
        done.set(true);
    }

    public boolean isDone() {
        return done.get();
    }

    public void loadModel(String url) {
        running.set(true);
    }
    
    public abstract Mesh getMesh();
}
