package main.asset.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import main.asset.Mesh;

public class ObjModelLoader {

    private static ArrayList<IndexBundle> indicesList = new ArrayList<IndexBundle>();
    private static int currentObject = 0;

    public static class IndexBundle {

        public IntList vertexIndices;
        public IntList textureIndices;
        public IntList normalIndices;

        public IndexBundle(int faces) {
            vertexIndices = new IntList(faces * 3);
            textureIndices = new IntList(faces * 3);
            normalIndices = new IntList(faces * 3);
        }
    }

    public static Mesh[] load(String location) {
        String file = loadFile(location);
        Scanner scan = new Scanner(file);
        String[] objectStrings = file.split("\no ");
        if (objectStrings.length > 1) {
            for (int i = 1; i < objectStrings.length; i++) {
                indicesList.add(new IndexBundle(objectStrings[i].split("\nf ").length - 1));
            }
            currentObject = -1;
        } else {
            indicesList.add(new IndexBundle(file.split("\nf ").length - 1));
            currentObject = 0;
        }

        FloatList vertices = new FloatList();
        FloatList textures = new FloatList();
        FloatList normals = new FloatList();

        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            line = line.trim();
            if (line.charAt(0) == '#') {
                //Handle Comment
            } else if (line.charAt(0) == 'v') {
                if (line.charAt(1) != 't' && line.charAt(1) != 'n' && line.charAt(1) != 'p') {
                    parseVertex(vertices, line.substring(1).trim());
                } else if (line.charAt(1) == 't') {
                    parseTexture(textures, line.substring(2).trim());
                } else if (line.charAt(1) == 'n') {
                    parseNormal(normals, line.substring(2).trim());
                }
            } else if (line.charAt(0) == 'f') {
                parseFace(line.substring(1).trim());
            } else if (line.charAt(0) == 'g') {
                //Handle Group Tag
            } else if (line.charAt(0) == 'o') {
                currentObject++;
            } else if (line.charAt(0) == 'u') {
                //Handle Library Usage
            } else if (line.charAt(0) == 'm') {
                //Handle Library Import
            }
        }

        Mesh[] results = new Mesh[indicesList.size()];
        for (int i = 0; i < results.length; i++) {
            if (indicesList.get(i).textureIndices.get(0) == -1) {
                results[0] = ModelLoaderUtils.combineIndicesWithoutTextures(indicesList.get(i),
                        vertices.getArray(), normals.getArray());
            } else {
                results[0] = ModelLoaderUtils.combineIndicesWithTextures(indicesList.get(i),
                        vertices.getArray(), textures.getArray(), normals.getArray());
            }
        }
        return results;
    }

    private static void parseVertex(FloatList vertices, String value) {
        String[] s = value.split(" ");
        vertices.add(Float.parseFloat(s[0]));
        vertices.add(Float.parseFloat(s[1]));
        vertices.add(Float.parseFloat(s[2]));
        vertices.add(1.0f);
    }

    private static void parseTexture(FloatList textures, String value) {
        String[] s = value.split(" ");
        if (s.length == 2) {
            textures.add(Float.parseFloat(s[0]));
            textures.add(Float.parseFloat(s[1]));
            textures.add(0.0f);
            textures.add(1.0f);
        } else {
            textures.add(Float.parseFloat(s[0]));
            textures.add(Float.parseFloat(s[1]));
            textures.add(Float.parseFloat(s[2]));
            textures.add(1.0f);
        }
    }

    private static void parseNormal(FloatList normals, String value) {
        String[] s = value.split(" ");
        normals.add(Float.parseFloat(s[0]));
        normals.add(Float.parseFloat(s[1]));
        normals.add(Float.parseFloat(s[2]));
        normals.add(1.0f);
    }

    private static void parseFace(String value) {
        String[] s = value.split(" ");
        if (s.length == 3) {
            parseFaceGroup(s[0]);
            parseFaceGroup(s[1]);
            parseFaceGroup(s[2]);
        } else if (s.length == 4) {
            parseFaceGroup(s[0]);
            parseFaceGroup(s[1]);
            parseFaceGroup(s[2]);

            parseFaceGroup(s[0]);
            parseFaceGroup(s[2]);
            parseFaceGroup(s[3]);
        }
    }

    private static void parseFaceGroup(String value) {
        String[] s = value.split("/");
        IndexBundle object = indicesList.get(currentObject);
        if (s.length == 1) { //Vertex
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(-1);
            object.normalIndices.add(-1);
        } else if (s.length == 2) { //Vertex, Texture
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(Integer.parseInt(s[1]) - 1);
            object.normalIndices.add(-1);
        } else if (s[1].length() == 0) { //Vertex, Normal
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(-1);
            object.normalIndices.add(Integer.parseInt(s[2]) - 1);
        } else { //Vertex, Normal, Texture
            object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
            object.textureIndices.add(Integer.parseInt(s[1]) - 1);
            object.normalIndices.add(Integer.parseInt(s[2]) - 1);
        }
    }

    public static String loadFile(String url) {
        StringBuilder builder = new StringBuilder();
        BufferedReader file = null;
        try {
            file = new BufferedReader(new FileReader(url));
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
            return null;
        }
        try {
            String temp;
            while ((temp = file.readLine()) != null) {
                if (temp.length() > 0) {
                    builder.append(temp).append("\n");
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
            return null;
        }
        return builder.toString();
    }
}
