package input;

import java.util.ArrayList;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

/**
 * @author David Gronlund
 */
public class Input implements Runnable {

    private Thread thread;
    private ArrayList<MouseListener> mouseListeners;
    private ArrayList<KeyListener> keyListeners;

    public Input() {
        thread = new Thread(this);
        mouseListeners = new ArrayList<MouseListener>();
        keyListeners = new ArrayList<KeyListener>();
    }

    public void start() {
        thread.start();
    }

    public void addMouseListener(MouseListener listener) {
        mouseListeners.add(listener);
    }

    public void addKeyListener(KeyListener listener) {
        keyListeners.add(listener);
    }

    @Override
    public void run() {
        while (!Display.isCloseRequested()) {
            if (Mouse.next()) {
                for (MouseListener l : mouseListeners) {
                    l.onEvent(new MouseEvent());
                }
            }
            if (Keyboard.next()) {
                for (KeyListener l : keyListeners) {
                    l.onEvent(new KeyEvent());
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
            }
        }
    }
}
